<?php
require "include.php";
$rs = new DBHelper();
$res = $rs->fetch_all("SHOW COLUMNS FROM `{$_GET['type']}`");
$sql = "select * from {$_GET['type']}";
$result = $rs->fetch_all($sql);
header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
header("Content-Disposition:filename={$_GET['type']}.xls");
$rt = array();
if ($res instanceof mysqli_result)
{
    while (($row = $res->fetch_assoc()) != FALSE)
    {
        $row['CanBeNull'] = $row['Null'] === 'YES';   //字段值是否可以为空，是的话值为'YES'
        $rt[] = $row;
    }
}
$ss=count($rt);
/*打印标题*/
for ($i = 0; $i < $ss; $i++) {
    echo iconv("UTF-8", "gb2312",$rt[$i]['Field'])."\t";
}
echo "\n";
/*打印内容*/
while ($arr = mysqli_fetch_array($result)) {
    for ($i = 0; $i < $ss; $i++) {
        echo iconv("UTF-8", "gb2312",$arr[$i])."\t";
    }
    echo "\n";
}
