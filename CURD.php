<?php
/**
 * Created by PhpStorm.
 * User: 苏徐
 * Date: 2017/10/30
 * Time: 14:49
 */
require_once 'include.php';
class DBHelper{
    public $type;
    public $id;
    public $tablename;
    public $where;
    private static $link = null;
    public static function open(){
        if(empty(self::$link)){
            self::$link = mysqli_connect("localhost","root","","excel");
        }
        mysqli_query(self::$link,"set names utf8");
    }
    //返回mysqli_fetch_assoc一条信息
    public static function query_one($sql){
        self::open();
        $result = mysqli_query(self::$link,$sql);
        $n=mysqli_fetch_assoc($result);
        return $n;
    }
    //返回mysqli_fetch_all所有信息
    public static function query($sql){
        self::open();
        $result = mysqli_query(self::$link,$sql);
        if($result){
            return true;
        }
        //$n=mysqli_fetch_all($result,MYSQLI_ASSOC);
        //return Response::show(200,'success',$n);
    }
    public function update($table,$array,$where=null){
        $str="";
        foreach($array as $key=>$val){
            if($str==null){
                $sep="";
            }else{
                $sep=",";
            }
            $str.=$sep.$key."='".$val."'";
        }
        $sql="update {$table} set {$str} ".($where==null?null:" where ".$where);
        $result=$this->query($sql);
        if($result){
            //return mysqli_affected_rows($this->conn);
            echo "修改数据成功";
        }else{
            echo "修改数据失败";
        }
    }
    //返回mysqli_fetch_all所有信息
    public static function query_t($sql){
        self::open();
        $result = mysqli_query(self::$link,$sql);
        if($result){
            echo "创建数据表成功";
        }else{
            echo "创建数据表失败";
        }
    }
    //返回记录条数
    public static function find_all($sql){
        self::open();
        $result = mysqli_query(self::$link,$sql);
        $total = mysqli_num_rows($result);
        return $total;
    }
    //返回mysqli_query结果集
    public static function fetch_all($sql){
        self::open();
        $result = mysqli_query(self::$link,$sql);
        return $result;
    }
    public static function insert($tableName, $column = array())
    {
        self::open();
        $columnName = "";
        $columnValue = "";
        foreach ($column as $key => $value) {
            $columnName .= $key . ",";
            $columnValue .= "'" . $value . "',";
        }
        $columnName = substr($columnName, 0, strlen($columnName) - 1);
        $columnValue = substr($columnValue, 0, strlen($columnValue) - 1);
        $sql = "INSERT INTO $tableName($columnName) VALUES($columnValue)";
        $result = mysqli_query(self::$link,$sql);
        if ($result) {
            echo "插入数据成功";
        }else{
            echo "插入数据失败";
        }
    }
    //删除数据
    public function delete(){
        $sql = "delete from $this->type where id = $this->id";
        $result = $this->query($sql);
        if($result){
            echo "删除数据成功";
        }
    }
    //删除数据表
    public function form_del(){
        $sql = "DROP TABLE {$this->type}";
        $result = $this->query($sql);
        if($result){
            $sql1="delete from type where type='{$this->type}'";
            $result1 = $this->query($sql1);
            if($result1){
                echo "删除数据表成功";
            }
        }
    }
    //不返回任何参数
    public function update_t($table,$array,$where=null){
        self::open();
        $str="";
        foreach($array as $key=>$val){
            if($str==null){
                $sep="";
            }else{
                $sep=",";
            }
            $str.=$sep.$key."='".$val."'";
        }
        $sql="update {$table} set {$str} ".($where==null?null:" where ".$where);
        mysqli_query(self::$link,$sql);
    }
    public static function total($sql)
    {
        $result = self::find_all($sql);
        $total = mysqli_num_rows($result);
        var_dump($total);
        exit();
    }
    public static function close()
    {
        //$result = mysql_close(self::$link);
        //var_dump($result);
        var_dump(self::$link);
    }
}

?>