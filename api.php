<?php
require_once 'include.php';
API::request();
class API{

    public static function request(){
        $parameters = array();
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $parameters = $_POST;/*接收传递过来的post数据*/
        }
        else{
            array_shift($_GET);
            $parameters = $_GET;
        }
        call_user_func_array(array("API",$_REQUEST['act']),$parameters);/*执行方法*/
    }
    //导入文字信息
    public static function input_word(){
        $rs = new DBHelper();
        $tablename=$_GET['type'];
        $array = $_POST;
        $rs->insert($tablename,$array);
    }
    //delete
    public static function delete(){
        $rs = new DBHelper();
        $rs->type=$_GET['type'];
        $rs->id=$_GET['id'];
        $rs->delete();
    }
    //删除表
    public static function form_del(){
        $rs = new DBHelper();
        $rs->type=$_GET['type'];
        $rs->form_del();
    }
    //update
    public static function update(){
        $rs = new DBHelper();
        $tablename=$_GET['type'];
        $array = $_POST;
        $where = "id={$_GET['id']}";
        $rs->update($tablename,$array,$where);
    }
}